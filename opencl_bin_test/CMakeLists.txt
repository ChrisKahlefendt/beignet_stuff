cmake_minimum_required(VERSION 3.1)
project(opencl_test)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -I ../src -I ../include -I /usr/include -I /usr/local/include")

#find_library(libcl NAMES cl HINTS "/home/chris/Documents/worktree/beignet_test/opencl_test/lib")
#find_package(OpenCL REQUIRED)
#MESSAGE(STATUS "LIBCL:" ${OpenCL_LIBRARY})
#MESSAGE(STATUS "LIBCL:" ${OpenCL_INCLUDE_DIR})

set(CTC_ROOTDIR "/home/nicolas/repos/hulks/cross_toolchains/ctc-linux64-hulks-8.2.0-1/root/")
set(CTC_LIBROOT "${CTC_ROOTDIR}/libroot")

add_executable(opencl_test src/main.cpp )
# target_link_libraries(opencl_test ${OpenCL_LIBRARY})
target_link_libraries(opencl_test "${CTC_LIBROOT}/usr/lib64/beignet/libcl.so")
target_include_directories(opencl_test PRIVATE "${CTC_LIBROOT}/usr/include")
#target_compile_options(opencl_test PRIVATE "-dump-opt-llvm=/home/nicolas/test.llvm")

file(GLOB GPU_KERNEL_PATHS ${CMAKE_SOURCE_DIR}/src/gpu_kernels/*.cl)

if(GPU_KERNEL_PATHS)
    message(STATUS "Collected GPU kernels:")

    # Add a target for creating the gpu_kernels folder inside the build folder
    add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/gpu_kernels
                       COMMAND mkdir ${CMAKE_BINARY_DIR}/gpu_kernels)
    add_custom_target(gpu_kernels_folder
                      DEPENDS ${CMAKE_BINARY_DIR}/gpu_kernels)

    # Create commands for each gpu kernel
    foreach(KERNEL_PATH ${GPU_KERNEL_PATHS})
        get_filename_component(KERNEL_BASE_NAME ${KERNEL_PATH} NAME_WE)
        set(KERNEL_BIN_PATH ${CMAKE_BINARY_DIR}/gpu_kernels/${KERNEL_BASE_NAME}.bin)
        message(STATUS "  - ${KERNEL_PATH}")
        message(STATUS "  - KERNEL BIN PATH ${KERNEL_BIN_PATH}")
        add_custom_command(OUTPUT ${KERNEL_BIN_PATH}
                           DEPENDS gpu_kernels_folder
                           COMMAND OCL_SYSROOT_DIR=${CTC_ROOTDIR} OCL_BITCODE_LIB_PATH=${CTC_ROOTDIR}/lib/beignet/beignet.bc OCL_HEADER_FILE_DIR=${CTC_ROOTDIR}/lib/beignet/include OCL_PCH_PATH=${CTC_ROOTDIR}/lib/beignet/beignet.pch ${CTC_ROOTDIR}/lib/beignet/gbe_bin_generater ${KERNEL_PATH} -o ${KERNEL_BIN_PATH} -p"-cl-std=CL1.2" -t 0x0f31)
        list(APPEND KERNEL_COMMANDS ${KERNEL_BIN_PATH})
    endforeach()

    # Add target for building the gpu_kernels
    add_custom_target(gpu_kernels
                      DEPENDS ${KERNEL_COMMANDS})
    add_dependencies(opencl_test gpu_kernels_folder gpu_kernels)
endif()

set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
