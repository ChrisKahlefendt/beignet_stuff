#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)

int main(void) {
    printf("Starting opencl_test\n");
    printf("Sizeof cl_int: %d\n", sizeof(cl_int));
    printf("Sizeof cl_mem: %d\n", sizeof(cl_mem));
    // Create the two input vectors
    int i;
    const int LIST_SIZE = 1024;
    cl_int *A = (cl_int *)malloc(sizeof(cl_int) * LIST_SIZE);
    cl_int *B = (cl_int *)malloc(sizeof(cl_int) * LIST_SIZE);
    for (i = 0; i < LIST_SIZE; i++) {
        A[i] = i;
        B[i] = LIST_SIZE - i;
    }

    // Load the kernel source code into the array source_str
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("gpu_kernels/vector_add_kernel.bin", "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel.\n");
        exit(1);
    }
    source_str = (char *)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    // Get platform and device information
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id,
                         &ret_num_devices);
    printf("Ret: %d\n", ret);

    // Create an OpenCL context
    cl_context context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

    // Create a command queue
    cl_command_queue command_queue =
        clCreateCommandQueue(context, device_id, 0, &ret);
    printf("Ret: %d\n", ret);

    // Create memory buffers on the device for each vector
    cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                      LIST_SIZE * sizeof(cl_int), NULL, &ret);
    printf("Ret: %d\n", ret);
    cl_mem b_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                      LIST_SIZE * sizeof(cl_int), NULL, &ret);
    printf("Ret: %d\n", ret);
    cl_mem c_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                                      LIST_SIZE * sizeof(cl_int), NULL, &ret);
    printf("Ret: %d\n", ret);

    // Copy the lists A and B to their respective memory buffers
    ret = clEnqueueWriteBuffer(command_queue, a_mem_obj, CL_TRUE, 0,
                               LIST_SIZE * sizeof(cl_int), A, 0, NULL, NULL);
    printf("Ret: %d\n", ret);
    ret = clEnqueueWriteBuffer(command_queue, b_mem_obj, CL_TRUE, 0,
                               LIST_SIZE * sizeof(cl_int), B, 0, NULL, NULL);
    printf("Ret: %d\n", ret);

    // Create a program from the kernel source
    // cl_program program = clCreateProgramWithSource(context, 1,
    //        (const char **)&source_str, (const size_t *)&source_size, &ret);
    cl_int binary_status;
    cl_program program = clCreateProgramWithBinary(
        context, 1, &device_id, (const size_t *)&source_size,
        (const unsigned char **)&source_str, &binary_status, &ret);
    printf("Ret: %d\n", ret);

    // Build the program
    ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
    printf("Ret: %d\n", ret);

    std::cout << "Return: " << ret << std::endl;

    // Create the OpenCL kernel
    cl_kernel kernel = clCreateKernel(program, "vector_add", &ret);
    printf("Ret: %d\n", ret);

    // Set the arguments of the kernel
    ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&a_mem_obj);
    printf("Ret: %d\n", ret);
    ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&b_mem_obj);
    printf("Ret: %d\n", ret);
    ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&c_mem_obj);
    printf("Ret: %d\n", ret);

    // Execute the OpenCL kernel on the list
    size_t global_item_size = LIST_SIZE;  // Process the entire lists
    size_t local_item_size = 64;          // Divide work items into groups of 64
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,
                                 &global_item_size, &local_item_size, 0, NULL,
                                 NULL);
    printf("Ret: %d\n", ret);

    // Wait for the command queue to get serviced before reading back results
    clFinish(command_queue);

    // Read the memory buffer C on the device to the local variable C
    cl_int *C = (cl_int *)malloc(sizeof(cl_int) * LIST_SIZE);
    ret = clEnqueueReadBuffer(command_queue, c_mem_obj, CL_TRUE, 0,
                              LIST_SIZE * sizeof(cl_int), C, 0, NULL, NULL);
    printf("Ret: %d\n", ret);

    // Display the result to the screen
    for (i = 0; i < LIST_SIZE; i++) printf("%d + %d = %d\n", A[i], B[i], C[i]);

    // Clean up
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);
    ret = clReleaseKernel(kernel);
    ret = clReleaseProgram(program);
    ret = clReleaseMemObject(a_mem_obj);
    ret = clReleaseMemObject(b_mem_obj);
    ret = clReleaseMemObject(c_mem_obj);
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);
    free(A);
    free(B);
    free(C);
    return 0;
}
